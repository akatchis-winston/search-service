package io.codekrijger.winston.service.search.rest.vm;

import io.codekrijger.winston.service.search.rest.controllers.SearchRestController;

/**
 * This object contains the result for the {@link SearchRestController}
 * which can then be returned to the user.
 */
public class SearchResultVM
{
    private String query;
    private String result;

    public SearchResultVM(String query, String result)
    {
        this.query = query;
        this.result = result;
    }

    public String getQuery()
    {
        return query;
    }

    public String getResult()
    {
        return result;
    }
}
