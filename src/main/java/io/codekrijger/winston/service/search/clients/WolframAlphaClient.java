package io.codekrijger.winston.service.search.clients;

import feign.Response;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * A feign client designed to communicate with the WolframAlpha API
 */
@FeignClient(name = "wolfram-alpha", url = "${wolfram-alpha.url}")
public interface WolframAlphaClient
{
    /**
     * This method queries the Short Answer API and then returns
     * the result from that API in a {@link Response} object
     *
     * <a url="https://products.wolframalpha.com/short-answers-api/documentation/">Documentation</a>
     *
     * @param appId The appId to authenticate with the WolframAlpha API
     * @param query The query to execute in the API
     *
     * @return The result of the API
     */
    @RequestMapping(method = RequestMethod.GET, value = "result?appid={appId}&i={query}")
    Response shortAnswerQuery(
            @PathVariable("appId") String appId,
            @PathVariable("query") String query
    );
}
