package io.codekrijger.winston.service.search.rest.controllers;

import feign.Response;
import io.codekrijger.winston.service.search.clients.WolframAlphaClient;
import io.codekrijger.winston.service.search.rest.vm.SearchResultVM;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * The goal of the SearchRestController is to expose endpoints which can be used
 * by consumers to search for a specific query.
 */
@RestController
@RequestMapping("/api/search")
public class SearchRestController
{
    @Value("${wolfram-alpha.app_id}")
    private String appId;

    private WolframAlphaClient client;

    @Autowired
    public SearchRestController(WolframAlphaClient client)
    {
        this.client = client;
    }

    /**
     * This endpoint can be used to resolve a given query to a result (if any).
     *
     * @param query The query which needs to be resolved
     *
     * @return The result (if any) based on the given query
     */
    @GetMapping
    public SearchResultVM search(@RequestParam(value = "query") String query) throws IOException
    {
        // TODO add error handling whenever the API returns 500/400 etc
        Response response = client.shortAnswerQuery(appId, query);
        String result = IOUtils.toString(response.body().asInputStream(), "UTF-8");

        return new SearchResultVM(query, result);
    }
}
